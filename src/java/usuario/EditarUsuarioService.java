
package usuario;

import conexao.Conexao;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Kaio
 */
public class EditarUsuarioService {
        
    /**
     * Método responsável por atualizar o usuário e seu contato no banco de dados.
     * @param usuario
     * @param contato
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public void editarUsuarioEContato(Usuario usuario, Contato contato) throws SQLException, ClassNotFoundException {
        try (Connection conn = Conexao.conectar()) {
            conn.setAutoCommit(false);

            UsuarioDAO usuarioDAO = new UsuarioDAO();
            boolean usuarioAtualizado = usuarioDAO.atualizarUsuario(conn, usuario);
            if (!usuarioAtualizado) {
                conn.rollback();
                throw new SQLException("Falha ao atualizar usuário");
            }
            ContatoDAO contatoDAO = new ContatoDAO();
            // Verifica se o contato já existe
            boolean contatoExiste = contatoDAO.existeContato(conn, usuario.getId());
            if (contatoExiste) {
                // Atualiza contato existente
                boolean contatoAtualizado = contatoDAO.atualizarContato(conn, contato);
                if (!contatoAtualizado) {
                    conn.rollback();
                    throw new SQLException("Falha ao atualizar contato");
                }
            } else {
                // Insere novo contato
                boolean contatoInserido = contatoDAO.criarContato(conn, contato);
                if (!contatoInserido) {
                    conn.rollback();
                    throw new SQLException("Falha ao inserir contato");
                }
            }

            conn.commit();
        } catch(SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    /**
     * Responsável por buscar um Usuário pelo ID e gerenciar a conexão com o BD
     * @param id
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public Usuario buscarUsuarioPorId(int id) throws SQLException, ClassNotFoundException {
        try (Connection conn = Conexao.conectar()) {
            UsuarioDAO usuarioDAO = new UsuarioDAO();
            return usuarioDAO.buscarUsuarioPorId(conn, id);
        }
    }
    
    /**
     * Responsável por buscar contato de um usuário pelo ID e gerenciar a conexão com o BD
     * @param usuarioId
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public Contato buscarContatoPorUsuarioId(int usuarioId) throws SQLException, ClassNotFoundException {        
        try (Connection conn = Conexao.conectar()) {
            ContatoDAO contatoDAO = new ContatoDAO();
            return contatoDAO.buscarContatoPorUsuarioId(conn, usuarioId);
        }
    }
}
