package usuario;

import conexao.Conexao;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Kaio
 */
public class CriarUsuarioService {
    
    public void criarUsuarioEContato(Usuario usuario, Contato contato) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        try {
            conn = Conexao.conectar();
            conn.setAutoCommit(false); // Inicia a transação

            UsuarioDAO usuarioDAO = new UsuarioDAO();
            int idUsuario = usuarioDAO.criarusuario(conn, usuario); // Insere o usuário e obtém o ID

            if (idUsuario > 0) {
                ContatoDAO contatoDAO = new ContatoDAO();
                contato.setUsuarioId(usuario); 
                contatoDAO.criarContato(conn, contato); // Insere o contato
                conn.commit(); // Finaliza a transação com sucesso
            } else {
                throw new SQLException("Falha ao inserir usuário");
            }
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    conn.rollback(); // Tenta fazer rollback se houver uma SQLException
                } catch (SQLException ex) {
                    // Se o rollback falhar, tratar a exceção do rollback aqui
                }
            }
            throw e; // Propaga a exceção
        } finally {
            if (conn != null) {
                try {
                    conn.close(); // Tenta fechar a conexão
                } catch (SQLException ex) {
                    // Tratamento da exceção de fechamento de conexão
                }
            }
        }
    }
    
}
