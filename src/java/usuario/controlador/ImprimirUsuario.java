package usuario.controlador;

import conexao.Conexao;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImprimirUsuario extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        try (Connection conn = Conexao.conectar()){
            // Obter o caminho real no contexto do servidor
            String reportPath = getServletContext().getRealPath("/relatorios/RelatorioProjetoListar.jrxml");
            
            // Compilar o relatório JRXML
            JasperReport jasperReport = JasperCompileManager.compileReport(reportPath);
            
            Map<String, Object> parametros = new HashMap<>();
            String path = getServletContext().getRealPath("/imagens/logo.png");
            parametros.put("logoPath", path);

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, conn);


            // Preparar a resposta HTTP para o PDF
            response.setContentType("application/pdf");
            //response.setHeader("Content-Disposition", "inline; filename=relatorio_usuarios.pdf"); //Abre no navegador
            response.setHeader("Content-Disposition", "attachment; filename=relatorio_usuarios.pdf"); //Força o Download


            // Exportar o relatório para PDF e escrever na resposta
            JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
            
        } catch (JRException | SQLException e) {
            throw new ServletException("Erro ao gerar o relatório: " + e.getMessage(), e);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ImprimirUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Implementação, se necessário
    }
}
