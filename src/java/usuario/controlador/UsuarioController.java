
package usuario.controlador;

/**
 *
 * @author Kaio
 */
import conexao.Conexao;
import usuario.UsuarioDAO;
import usuario.Usuario;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;

@WebServlet("/listarUsuarios")
public class UsuarioController extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        Connection conn = null;
        
        UsuarioDAO dao = new UsuarioDAO();
        List<Usuario> usuarios;
        
        int start = 0;
        int length = 10; // Valor padrão

        String startParam = request.getParameter("start");
        String lengthParam = request.getParameter("length");

        if (startParam != null && !startParam.isEmpty()) {
            start = Integer.parseInt(startParam);
        }

        if (lengthParam != null && !lengthParam.isEmpty()) {
            length = Integer.parseInt(lengthParam);
        }

        
        try {
            conn = Conexao.conectar();
            //usuarios = dao.listarTodos(conn); //Método de listagem convencional
            usuarios = dao.buscarUsuariosPaginados(conn, start, length);
            request.setAttribute("usuarios", usuarios);
            RequestDispatcher dispatcher = request.getRequestDispatcher("lista.jsp");
            dispatcher.forward(request, response);
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
}
