/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario.controlador;

import conexao.Conexao;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import usuario.Usuario;
import usuario.UsuarioDAO;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 *
 * @author Kaio
 */
@WebServlet(name = "ListUserDataTables", urlPatterns = {"/ListUserDataTables"})
public class ListUserDataTables extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Connection conn = null;

        
        try {
            
            int start = 0; // Valor padrão
            int length = 10; // Valor padrão

            String startParam = request.getParameter("start");
            String lengthParam = request.getParameter("length");

            if (startParam != null && startParam.matches("\\d+")) { // Verifica se é um número
                start = Integer.parseInt(startParam);
            }

            if (lengthParam != null && lengthParam.matches("\\d+")) { // Verifica se é um número
                length = Integer.parseInt(lengthParam);
            }
            
            conn = Conexao.conectar();
            // Buscar os dados paginados
            UsuarioDAO dao = new UsuarioDAO();
            List<Usuario> usuarios = dao.buscarUsuariosPaginados(conn, start, length); 
            
            // Criar o JSON de resposta
            JsonObject jsonResponse = new JsonObject();
            String drawParam = request.getParameter("draw");
            int draw = 1; // Um valor padrão, caso não seja fornecido

            if (drawParam != null && drawParam.matches("\\d+")) {
                draw = Integer.parseInt(drawParam);
            }

            jsonResponse.addProperty("draw", draw);

            //jsonResponse.addProperty("draw", Integer.parseInt(request.getParameter("draw")));
            jsonResponse.addProperty("recordsTotal", usuarios.size()); 
            jsonResponse.addProperty("recordsFiltered", usuarios.size()); 

            JsonArray data = new JsonArray();
            for (Usuario usuario : usuarios) {
                JsonArray row = new JsonArray();
                row.add(usuario.getNome());
                row.add(usuario.getIdade());
                // Adicione outras colunas conforme necessário
                data.add(row);
            }
            jsonResponse.add("data", data);
            // Configurar e enviar a resposta
            response.setContentType("application/json");
            response.getWriter().write(jsonResponse.toString());
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ListUserDataTables.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    
                }
            }
        }
        




    }

}
