package usuario.controlador;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import usuario.Contato;
import usuario.CriarUsuarioService;
import usuario.Usuario;


/**
 *
 * @author Kaio
 */
public class CriarUsuarioController extends HttpServlet {

   
    
    /**
     * Responsável por direcionar para página de criação de usuário.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("criarusuario.jsp");
        dispatcher.forward(request, response);
       
    }
    
    /**
     * Responsável por criar um usuário e inserir no banco de dados.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Pegando os valores vindos dos inputs
        String nome = request.getParameter("nome");
        int idade = Integer.parseInt(request.getParameter("idade"));
        String telefone = request.getParameter("telefone");
        String email = request.getParameter("email");
        
        //Criando objeto da Classe model Usuario e definindo os atributos com os valores dos inputs
        Usuario usuarioObj = new Usuario();
        usuarioObj.setNome(nome);
        usuarioObj.setIdade(idade);
        
        //Criando objeto da Classe model Contato e definindo os atributos com os valores dos inputs
        Contato contatoObj = new Contato();
        contatoObj.setTelefone(telefone);
        contatoObj.setEmail(email);
        
        //Chamando método da classe de Serviço para inserir os registros no banco de dados
        
        CriarUsuarioService criarUsuarioService = new CriarUsuarioService();
        try {
            criarUsuarioService.criarUsuarioEContato(usuarioObj, contatoObj);
            request.getSession().setAttribute("successMessage", "Usuário criado com sucesso!");
            response.sendRedirect("listarUsuarios");
        } catch (IOException | ClassNotFoundException | SQLException e) {
        // Tratamento de exceção adequado
            request.getSession().setAttribute("errorMessage", "Erro ao criar usuário!");
            response.sendRedirect("erro.jsp"); // Ou a página de erro apropriada
        }
        
       
    }



}
