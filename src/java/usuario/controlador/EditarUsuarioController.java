package usuario.controlador;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import usuario.Contato;
import usuario.EditarUsuarioService;
import usuario.Usuario;


/**
 *
 * @author Kaio
 */
public class EditarUsuarioController extends HttpServlet {


    private final EditarUsuarioService editarUsuarioService;

    public EditarUsuarioController() {
        this.editarUsuarioService = new EditarUsuarioService();
    }

    @Override
    /**
     * O método doGet é responsável por buscar um usuário e seu contato para edição e, então, 
     * encaminhar para a página JSP correspondente com essas informações.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //Pegando o ID do Usuário
        int id = Integer.parseInt(request.getParameter("id"));
        
        try {
            Usuario usuario = editarUsuarioService.buscarUsuarioPorId(id);
            
            Contato contato = editarUsuarioService.buscarContatoPorUsuarioId(id);
            
            //Verifica se o usuário não está vazio
            if (usuario != null) {
                request.setAttribute("usuario", usuario);
                //Se o contato não existir, cria um objeto novo e define a sua chave estrangeira com o usuário.
                if (contato == null) {
                    contato = new Contato(); 
                    contato.setUsuarioId(usuario);
                }
                request.setAttribute("contato", contato);
                RequestDispatcher dispatcher = request.getRequestDispatcher("editarusuario.jsp");
                dispatcher.forward(request, response);
            } else {
                request.getSession().setAttribute("errorMessage", "Usuário ou contato não encontrado.");
                response.sendRedirect("listarUsuarios");
            }
        } catch (SQLException | ClassNotFoundException e) {
            response.sendRedirect("erro.jsp");
        }
    }

    @Override
    /**
     * O método doPost é responsável por receber os parâmetros do formulário, criar os objetos Usuario e Contato, 
     * e então chamar o serviço para atualizar as informações. Em seguida, ele trata o redirecionamento com base 
     * no sucesso ou falha da operação.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //Parâmetros do Usuário nos inputs
        int id = Integer.parseInt(request.getParameter("id"));
        String nome = request.getParameter("nome");
        int idade = Integer.parseInt(request.getParameter("idade"));
        
        //Parâmetros do Contato do usuário nos inputs 
        String telefone = request.getParameter("telefone");
        String email = request.getParameter("email");
        
        //Criando objeto do usuário
        Usuario usuarioObj = new Usuario();
        usuarioObj.setId(id); // Este ID é usado apenas para identificar o usuário, não para modificar
        usuarioObj.setNome(nome);
        usuarioObj.setIdade(idade);
        
        //Criando objeto do contato
        Contato contatoObj = new Contato();        
        contatoObj.setTelefone(telefone);
        contatoObj.setEmail(email);
        contatoObj.setUsuarioId(usuarioObj);   
        
        
        try {            
            //Chamando o método que atualiza o usuário e o contato enviando os objetos como parâmetro
            editarUsuarioService.editarUsuarioEContato(usuarioObj, contatoObj);
            
            //Atribuindo mensagem de sucesso para exibir na página após edição com sucesso.
            request.getSession().setAttribute("successMessage", "Usuário e contato atualizados com sucesso!");
            response.sendRedirect("listarUsuarios");
            
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            //Atribuindo mensagem de erro para exibir na página após edição com falha.
            request.getSession().setAttribute("errorMessage", "Erro ao atualizar usuário e contato: " + e.getMessage());
            response.sendRedirect("erro.jsp");
        }
    }


}
