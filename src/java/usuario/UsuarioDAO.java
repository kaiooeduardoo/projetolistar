package usuario;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;


/**
 *
 * @author Kaio
 */
public class UsuarioDAO {
    
    private static UsuarioDAO instancia;
    
    //Instância para o Singleton
    public static UsuarioDAO obterInstancia() {
        if (instancia == null) {
            instancia = new UsuarioDAO();
        }
        return instancia;
    }

    UsuarioDAO(Connection connn) {
        
    }

    public UsuarioDAO() {
        
    }
    
    /* Método responsável por listar todos usuários da tabela usuarios
     *
     */
    public List<Usuario> listarTodos(Connection conn) throws ClassNotFoundException {
        List<Usuario> usuarios = new ArrayList<>();

        String sql = "SELECT * FROM usuarios";

        try (PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery()) {
            
            while (rs.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(rs.getInt("id_usuario"));
                usuario.setNome(rs.getString("nome"));
                usuario.setIdade(rs.getInt("idade"));
                usuarios.add(usuario);                
            }
            
        } catch (SQLException e) {
            // O tratamento de exceção pode ser mais específico aqui
            throw new RuntimeException("Falha ao listar usuários", e);
        }
        // Recursos são fechados automaticamente pelo try-with-resources
        return usuarios;
    }
    
    /**
     * Método responsável por listar usuários de forma paginada sob demanda
     * 
     * @param conn
     * @param start
     * @param length
     * @return
     * @throws SQLException 
     */
    public List<Usuario> buscarUsuariosPaginados(Connection conn, int start, int length) throws SQLException {
        List<Usuario> listUsuarios = new ArrayList<>();

        String sql = "SELECT * FROM usuarios ORDER BY id_usuario LIMIT ? OFFSET ?";     

        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, length);
            statement.setInt(2, start);
            
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    //int id = resultSet.getInt("id");
                    String nome = resultSet.getString("nome");
                    Integer idade = resultSet.getInt("idade");
                    
                   // Usuario usuario = new Usuario(/*id,*/ nome, idade);
                    Usuario usuario = new Usuario();
                   // usuario.setId(resultSet.getInt("id_usuario"));
                    usuario.setNome(resultSet.getString("nome"));
                    usuario.setIdade(resultSet.getInt("idade"));

                    listUsuarios.add(usuario);
                }
            }
        }
       

        return listUsuarios;
    }
    
    /**
     * Método responsável por criar um novo usuário.
     * @param conn conexão com o BD
     * @param usuarioObj com valores vindo da view  
     * @return   
     * @throws java.sql.SQLException 
     */
    public int criarusuario(Connection conn, Usuario usuarioObj) throws SQLException {
        String sql = "INSERT INTO usuarios (nome, idade) VALUES (?, ?)";
        try (PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, usuarioObj.getNome());
            pstmt.setInt(2, usuarioObj.getIdade());

            int affectedRows = pstmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    usuarioObj.setId(generatedKeys.getInt(1)); // Atualiza o ID no objeto usuarioObj
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        }
        return usuarioObj.getId(); // Retorna o ID do usuário atualizado
    }
    
    /**
     * Método responsável por atualizar um usuário no banco de dados
     * @param conn
     * @param usuario
     * @return
     * @throws SQLException 
     */
    public boolean atualizarUsuario(Connection conn, Usuario usuario) throws SQLException {
        String sql = "UPDATE usuarios SET nome = ?, idade = ? WHERE id_usuario = ?";
        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, usuario.getNome());
            statement.setInt(2, usuario.getIdade());
            statement.setInt(3, usuario.getId());

            int rowsAffected = statement.executeUpdate();
            return rowsAffected > 0;
        }
    }
    
    /**
     * Buscar um usuário pelo seu ID.
     * @param conn
     * @param id
     * @return 
     */
    public Usuario buscarUsuarioPorId(Connection conn, int id) {
        String sql = "SELECT * FROM usuarios WHERE id_usuario = ?";
        Usuario usuario = null;

        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                usuario = new Usuario();
                usuario.setId(resultSet.getInt("id_usuario"));
                usuario.setNome(resultSet.getString("nome"));
                usuario.setIdade(resultSet.getInt("idade"));
                
            }
        } catch (SQLException e) {
        }

        return usuario;
    }

    
    
}
    

