
package usuario;

import java.sql.*;


/**
 *
 * @author Kaio
 */
public class ContatoDAO {

    ContatoDAO(Connection conn) {
        
    }

    ContatoDAO() {
        
    }
    
    /**
     * Responsável por criar um novo contato de um usuário.
     * @param conn
     * @param contatoObj 
     * @return 
     * @throws java.sql.SQLException 
     */
    public boolean criarContato(Connection conn, Contato contatoObj) throws SQLException {
        String sql = "INSERT INTO contato (telefone, email, usuario_id) VALUES (?, ?, ?)";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, contatoObj.getTelefone());
            pstmt.setString(2, contatoObj.getEmail());
            pstmt.setInt(3, contatoObj.getUsuarioId().getId()); // Aqui deve ser o ID do usuário

            int affectedRows = pstmt.executeUpdate();
            return affectedRows > 0;
        }
    }
    
    /**
     * Responsável por atualizar um contato já existente de um usuário.
     * @param conn
     * @param contatoObj
     * @return
     * @throws SQLException 
     */
    public boolean atualizarContato (Connection conn, Contato contatoObj) throws SQLException {
        String sql = "UPDATE contato SET telefone = ?, email = ? WHERE usuario_id = ?";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, contatoObj.getTelefone());
            pstmt.setString(2, contatoObj.getEmail());
            pstmt.setInt(3, contatoObj.getUsuarioId().getId());
            
            int affectedRows = pstmt.executeUpdate();
            return affectedRows > 0;
        }
    }
    
    /**
     * Verifica se existe contato para o usuário a ser editado.
     * @param conn
     * @param usuarioId
     * @return
     * @throws SQLException 
     */
    public boolean existeContato(Connection conn, int usuarioId) throws SQLException {
        String sql = "SELECT COUNT(1) FROM contato WHERE usuario_id = ?";
        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, usuarioId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1) > 0;
            }
        }
        return false;
    }

    /**
     * Busca um contato pela chave estrangeira, utilizando o id do usuário como parâmetro.
     * @param conn
     * @param usuarioId
     * @return 
     */
    public Contato buscarContatoPorUsuarioId(Connection conn, int usuarioId) {
        String sql = "SELECT * FROM contato WHERE usuario_id = ?";
        Contato contato = null;

        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, usuarioId);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                contato = new Contato();
                contato.setIdContato(resultSet.getInt("id_contato"));
                contato.setTelefone(resultSet.getString("telefone"));
                contato.setEmail(resultSet.getString("email"));
                
                // Aqui, você precisa criar um novo objeto Usuario com o ID e configurá-lo no contato
                Usuario usuario = new Usuario();
                usuario.setId(usuarioId); // Apenas o ID é necessário para o objeto Contato
                contato.setUsuarioId(usuario); 
               
            }
        } catch (SQLException e) { 
            e.printStackTrace();
        }

        return contato;
    }


}
