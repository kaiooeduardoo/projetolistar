/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo;

/**
 *
 * @author Kaio
 */
public class Veiculo {
    
    private int id_veiculo_locado;
    private String placa;
    private String modelo;
    
    //getters e setters

    public int getId_veiculo_locado() {
        return id_veiculo_locado;
    }

    public void setId_veiculo_locado(int id_veiculo_locado) {
        this.id_veiculo_locado = id_veiculo_locado;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    
    
}
