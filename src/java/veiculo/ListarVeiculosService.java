/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo;

import conexao.Conexao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kaio
 */
public class ListarVeiculosService {
    
    private static ListarVeiculosService instancia;
    
    //Instância para o Singleton
    public static ListarVeiculosService obterInstancia() {
        if (instancia == null) {
            instancia = new ListarVeiculosService();
        }
        return instancia;
    }
    
    /**
     * Lista todos os veículos da tabela veiculo_locado.
     * 
     * @return Uma lista contendo todos os veículos.
     * @throws SQLException Se ocorrer um erro de SQL.
     * @throws ClassNotFoundException Se a classe do driver do BD não for encontrada.
     */
    public List<Veiculo> listarVeiculos() throws SQLException, ClassNotFoundException {
        // Não é necessário desativar o auto-commit para operações de leitura.
        try (Connection conn = Conexao.conectar()) {
            // Utiliza a DAO para obter a lista de veículos
            return VeiculoDAO.obterInstancia().listarTodosVeiculos(conn);
        }
        // O tratamento de exceções SQLException já está sendo realizado dentro do DAO,
        // então ele será propagado daqui caso ocorra.
    }
}
