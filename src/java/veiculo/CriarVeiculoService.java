/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo;

import conexao.Conexao;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Kaio
 */
public class CriarVeiculoService {
    
    private static CriarVeiculoService instancia;
    
    //Instância para o Singleton
    public static CriarVeiculoService obterInstancia() {
        if (instancia == null) {
            instancia = new CriarVeiculoService();
        }
        return instancia;
    }
    
    /**
     * Cria um novo veículo no banco de dados.
     *
     * @param veiculo Objeto Veiculo contendo os dados a serem inseridos.
     * @throws SQLException Se ocorrer um erro durante o acesso ao banco de dados.
     * @throws ClassNotFoundException Se o driver do banco de dados não for encontrado.
     */
    public void criarVeiculo(Veiculo veiculo) throws SQLException, ClassNotFoundException {
        // Utiliza try-with-resources para garantir que a conexão seja fechada automaticamente
        try (Connection conn = Conexao.conectar()) {
            conn.setAutoCommit(false);
            
            // Aqui continua o bloco try-catch para controle de transações
            try {
                boolean sucesso = VeiculoDAO.obterInstancia().criarVeiculo(conn, veiculo);
                if (sucesso) {
                    conn.commit();
                } else {
                    conn.rollback();
                }
            } catch (SQLException ex) {
                // Em caso de exceção, tenta reverter as alterações
                conn.rollback();
                throw ex; // Relança a exceção para ser tratada ou logada em um nível superior
            }
        } // O recurso Connection é automaticamente fechado aqui
    }
    
}
