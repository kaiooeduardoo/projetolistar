/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Kaio
 */
public class VeiculoDAO {
    
    private static VeiculoDAO instancia;
    
    //Instância para o Singleton
    public static VeiculoDAO obterInstancia() {
        if (instancia == null) {
            instancia = new VeiculoDAO();
        }
        return instancia;
    }
    
    //Constructor empty
    public VeiculoDAO() {
    }
    
    /**
    * Cria um novo veículo no banco de dados.
    *
    * @param conn Conexão com o banco de dados.
    * @param veiculo Objeto Veiculo contendo os dados a serem inseridos.
    * @return true se o veículo foi criado com sucesso, false caso contrário.
    * @throws SQLException se ocorrer um erro durante a operação de banco de dados.
    */
   public boolean criarVeiculo(Connection conn, Veiculo veiculo) throws SQLException {
       // SQL para inserir um novo veículo
       String sql = "INSERT INTO veiculo_locado (placa, modelo) VALUES (?, ?)";

       try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
           // Define os parâmetros para o comando SQL
           pstmt.setString(1, veiculo.getPlaca());
           pstmt.setString(2, veiculo.getModelo());

           // Executa a inserção
           int affectedRows = pstmt.executeUpdate();

           // Verifica se foi inserido algum registro
           return affectedRows > 0;
       } catch (SQLException ex) {
           // Loga a exceção e lança novamente para ser tratada em um nível superior
           throw new SQLException("Falha ao criar veículo", ex);
       }
   }

    
    /**
     * Responsável por listar os veículos da tabela veiculo_locado
     * @param conn
     * @return
     * @throws SQLException 
     */
    public List<Veiculo> listarTodosVeiculos(Connection conn) throws SQLException {
        List<Veiculo> veiculos = new ArrayList<>();
        
        String sql = "SELECT * FROM veiculo_locado";
        
        try(PreparedStatement pstm = conn.prepareStatement(sql);
                ResultSet rs = pstm.executeQuery()){
            
            while (rs.next()) {
                Veiculo veiculo = new Veiculo();
                veiculo.setId_veiculo_locado(rs.getInt("id_veiculo_locado"));
                veiculo.setPlaca(rs.getString("placa"));
                veiculo.setModelo(rs.getString("modelo"));
                veiculos.add(veiculo);
            }
        } catch (SQLException ex){
            throw new RuntimeException("Falha ao tentar listar veículos!", ex);
        }
        return veiculos;
    }
    
    /**
     * Consultar um veículo pelo seu ID
     * @param conn
     * @param id
     * @return
     * @throws SQLException 
     */
    public Veiculo consultaVeiculoPorId (Connection conn, Integer id) throws SQLException {
        
        String sql = "SELECT * FROM veiculo_locado WHERE id_veiculo_locado = ?";
        Veiculo veiculo = null;
        try (PreparedStatement pstm = conn.prepareStatement(sql)) {
            pstm.setInt(1, id);
            try (ResultSet rs = pstm.executeQuery()) {
                if (rs.next()) {
                    veiculo = new Veiculo();
                    veiculo.setId_veiculo_locado(rs.getInt("id_veiculo_locado"));
                    veiculo.setPlaca(rs.getString("placa"));
                    veiculo.setModelo(rs.getString("modelo"));
                }
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        
        return veiculo;
    }
    
    
}
