/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import veiculo.ListarVeiculosService;
import veiculo.Veiculo;

/**
 *
 * @author Kaio
 */
@WebServlet(name = "ListarVeiculos", urlPatterns = {"/ListarVeiculos"})
public class ListarVeiculosController extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            // Obter a instância do serviço e listar os veículos
            List<Veiculo> veiculos = ListarVeiculosService.obterInstancia().listarVeiculos();
            
            // Adiciona a lista de veículos ao request para que possa ser acessada na JSP
            req.setAttribute("veiculos", veiculos);
            
            // Encaminha o request para a JSP
            req.getRequestDispatcher("/listaVeiculos.jsp").forward(req, resp);
        } catch (SQLException | ClassNotFoundException e) {
            // Log de erro e tratamento de exceção apropriado
            throw new ServletException("Erro ao listar veículos", e);
        }
    }
}
