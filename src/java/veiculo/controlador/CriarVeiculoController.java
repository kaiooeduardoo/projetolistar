/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculo.controlador;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import veiculo.CriarVeiculoService;
import veiculo.Veiculo;

/**
 *
 * @author Kaio
 */
public class CriarVeiculoController extends HttpServlet {
    
    /**
     * Responsável por direcionar para página de criação de veículo.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("CriarVeiculo.jsp");
        dispatcher.forward(request, response);
       
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String placa = request.getParameter("placa");
        String modelo = request.getParameter("modelo");
        
        Veiculo veiculo = new Veiculo();
        veiculo.setPlaca(placa);
        veiculo.setModelo(modelo);
        
        try{
            CriarVeiculoService.obterInstancia().criarVeiculo(veiculo);
            request.getSession().setAttribute("successMessage", "Veículo criado com sucesso!");
            response.sendRedirect("ListarVeiculos");
        } catch(Exception e){
            try {
                throw new Exception("Erro ao realizar a operação! ", e);
            } catch (Exception ex) {
                Logger.getLogger(CriarVeiculoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
