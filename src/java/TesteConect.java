import conexao.Conexao;
import java.sql.Connection;

public class TesteConect {
    
    public static void main(String[] args) throws ClassNotFoundException {
        
        Connection con = null;
        try {
            con = Conexao.conectar();
            if(con != null){
               System.out.println("Conexão realizada com sucesso!"); 
            } else {
               System.out.println("Falha ao conectar ao Banco de Dados!"); 
            }
        } catch (RuntimeException e) {
            System.out.println("Exceção capturada: " + e.getMessage());
        } finally {
            try {
                if(con != null) {
                    con.close();
                }
            } catch (Exception e) {
                System.out.println("Não foi possível fechar a conexão: " + e.getMessage());
            }
        }
    }
}
