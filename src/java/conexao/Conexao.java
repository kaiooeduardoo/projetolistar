
package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Kaio
 */


public class Conexao {
    
    private static final String URL = "jdbc:postgresql://localhost:5432/projetolistar";
    private static final String USER = "postgres";
    private static final String PASS = "postgres";
    
    private Conexao() {

    }

    public static Connection conectar() throws ClassNotFoundException {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException e) {
            throw new RuntimeException("Falha na conexão com o Banco de Dados!", e);
        }
    }
}
