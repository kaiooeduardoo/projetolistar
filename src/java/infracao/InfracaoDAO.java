/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infracao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import usuario.Usuario;
import usuario.UsuarioDAO;
import veiculo.Veiculo;
import veiculo.VeiculoDAO;


/**
 *
 * @author Kaio
 */
public class InfracaoDAO {
    
    private static InfracaoDAO instancia;
    
    //Instância para o Singleton
    public static InfracaoDAO obterInstancia() {
        if (instancia == null) {
            instancia = new InfracaoDAO();
        }
        return instancia;
    }
    
    //Constructor empty
    public InfracaoDAO() {
    }
    
    /**
     * Pesquisa veículos pela placa.
     * 
     * @param conn A conexão com o banco de dados.
     * @param placa A placa do veículo a ser pesquisada.
     * @return Uma lista de veículos que correspondem à placa fornecida.
     * @throws java.sql.SQLException
     */
    public List<Veiculo> pesquisarVeiculosPorPlaca(Connection conn, String placa) throws SQLException {
        List<Veiculo> veiculos = new ArrayList<>();
        String sql = "SELECT id_veiculo_locado, placa, modelo FROM veiculo_locado WHERE placa ILIKE ?";
        
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, "%" + placa + "%");
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    Veiculo veiculo = new Veiculo();
                    veiculo.setId_veiculo_locado(rs.getInt("id_veiculo_locado"));
                    veiculo.setPlaca(rs.getString("placa"));
                    veiculo.setModelo(rs.getString("modelo"));
                    veiculos.add(veiculo);
                }
            }
        }
        return veiculos;
    }

    /**
     * Cria uma nova infração no banco de dados.
     * 
     * @param conn A conexão com o banco de dados.
     * @param infracao A infração a ser criada.
     * @return true se a infração foi criada com sucesso, false caso contrário.
     * @throws java.sql.SQLException
     */
    public boolean criarInfracao(Connection conn, Infracao infracao) throws SQLException {
        String sql = "INSERT INTO infracoes (placa, veiculo_id, usuario_id, obs) VALUES (?, ?, ?, ?)";
        
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, infracao.getPlaca());
            pstmt.setInt(2, infracao.getVeiculo().getId_veiculo_locado());
            pstmt.setInt(3, infracao.getUsuario().getId());
            pstmt.setString(4, infracao.getObs());
            
            int affectedRows = pstmt.executeUpdate();
            return affectedRows > 0;
        }
    }

    
    /**
     * Responsável por listar as Infrações do banco de dados
     * @param conn
     * @return
     * @throws SQLException 
     */
    public List<Infracao> listarInfracoes(Connection conn) throws SQLException {
        List<Infracao> infracoes = new ArrayList<>();
        String sql = "SELECT * FROM infracoes";
        
        try(PreparedStatement pstmt = conn.prepareStatement(sql)) {
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    Infracao infracao = new Infracao();
                    infracao.setIdInfracao(rs.getInt("id_infracao"));
                    infracao.setPlaca(rs.getString("placa"));
                    infracao.setObs(rs.getString("obs"));
                    infracao.setVeiculo(VeiculoDAO.obterInstancia().consultaVeiculoPorId(conn, rs.getInt("veiculo_id")));
                    
                    infracao.setUsuario(UsuarioDAO.obterInstancia().buscarUsuarioPorId(conn, rs.getInt("usuario_id")));
                    
                    infracoes.add(infracao);
                }
            }            
        }
        return infracoes;
    }
    
}
