/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infracao;

import usuario.Usuario;
import veiculo.Veiculo;

/**
 *
 * @author Kaio
 */
public class Infracao {
    private int idInfracao;
    private String placa;
    private Veiculo veiculo;
    private Usuario usuario;
    private String obs;
    
    // Construtores, getters e setters
    public Infracao() {
    }

    public int getIdInfracao() {
        return idInfracao;
    }

    public void setIdInfracao(int idInfracao) {
        this.idInfracao = idInfracao;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }
}

