/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infracao.controlador;

import com.google.gson.Gson;
import conexao.Conexao;
import infracao.Infracao;
import infracao.InfracaoDAO;
import infracao.InfracaoService;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import usuario.Usuario;
import usuario.UsuarioDAO;
import veiculo.Veiculo;

/**
 *
 * @author Kaio
 */
@WebServlet(urlPatterns = {"/infracao", "/pesquisarVeiculo", "/listarInfracoes"})
public class InfracaoController extends HttpServlet {
    Connection conn = null;
    private InfracaoService infracaoService = new InfracaoService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getServletPath();
        
        if ("/pesquisarVeiculo".equals(path)) {
            // Lógica para pesquisar veículos pela placa e responder com JSON
            handlePesquisarVeiculo(req, resp);
            
        } else if ("/infracao".equals(path)) {
            // Exibir a página de criação de infração
            try{                
                conn = Conexao.conectar();
                List<Usuario> usuarios = UsuarioDAO.obterInstancia().listarTodos(conn);
                req.setAttribute("usuarios", usuarios);
                req.getRequestDispatcher("/CriarInfracao.jsp").forward(req, resp);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(InfracaoController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } else if ("/listarInfracoes".equals(path)) {
            //Exibir listagem de Infrações cadastradas
            try {
                conn = Conexao.conectar();
                List<Infracao> infracoes = InfracaoDAO.obterInstancia().listarInfracoes(conn);
                req.setAttribute("infracoes", infracoes);
                req.getRequestDispatcher("/listaInfracoes.jsp").forward(req, resp);
                
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(InfracaoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Lógica para criar uma nova infração baseada nos dados do formulário
        handleCriarInfracao(req, resp);
    }

    private void handlePesquisarVeiculo(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String placa = req.getParameter("placa");
        
        resp.setContentType("application/json");
                
        try {
            if (placa == null || placa.trim().isEmpty()) {                
                
                resp.getWriter().write("[]"); // Responde com uma lista vazia para não listar tudo
               
            }else{
               List<Veiculo> veiculos = infracaoService.pesquisarVeiculosPorPlaca(placa);
            
                // Usando Gson para converter a lista de veículos em uma string JSON
                String json = new Gson().toJson(veiculos);

                // Enviando a resposta JSON
                resp.getWriter().write(json); 
            }
        } catch (SQLException | ClassNotFoundException e) {
            // Tratamento de erro, enviando uma resposta de erro em JSON
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().write("{\"error\": \"Erro ao buscar veículos\"}");
        }
    }

    private void handleCriarInfracao(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {        
        //Parâmetros da página
        String veiculoIdStr = req.getParameter("veiculoId");
        String usuarioIdStr = req.getParameter("usuarioId");
        int veiculoId = 0;
        int usuarioId = 0;
        
        if(veiculoIdStr == null || veiculoIdStr.isEmpty() || usuarioIdStr == null || usuarioIdStr.isEmpty()){
            //não tentar converter em inteiro
        }else{
            veiculoId = Integer.parseInt(veiculoIdStr);
            usuarioId = Integer.parseInt(usuarioIdStr);
        }
        
        String placa = req.getParameter("placa");
        String obs = req.getParameter("obs");
        
        try {            
            
            //Validação para quando um veículo não for selecionado
            
            if (veiculoIdStr == null || veiculoIdStr.isEmpty()) {
                // Trata o caso em que o veículo não foi selecionado
                req.setAttribute("errorMessage", "Veículo não selecionado.");               
                //req.getRequestDispatcher("PaginaErro").forward(req, resp); // resp.sendRedirect("PaginaErro");
                resp.sendRedirect("PaginaErro");
                return;
            } else{
                //Definição dos objetos
                Veiculo veiculo = new Veiculo();
                veiculo.setId_veiculo_locado(veiculoId); 
                Usuario usuario = new Usuario();
                usuario.setId(usuarioId);
                Infracao infracao = new Infracao();
                infracao.setVeiculo(veiculo);
                infracao.setUsuario(usuario); 
                infracao.setPlaca(placa);
                infracao.setObs(obs);

                //Criar a infração
                infracaoService.criarInfracao(infracao);
                req.getSession().setAttribute("successMessage", "Infração cadastrada com sucesso!");
                resp.sendRedirect("listarInfracoes");
            }
            
            
        } catch (NumberFormatException | SQLException | ClassNotFoundException e) {
            // Tratar erro de conversão de número
            e.printStackTrace();
        }
        // Tratar exceção de banco de dados
        
    }

}

