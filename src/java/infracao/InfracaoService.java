/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infracao;

import conexao.Conexao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import veiculo.Veiculo;

/**
 *
 * @author Kaio
 */
public class InfracaoService {
    
    private InfracaoDAO infracaoDAO;

    public InfracaoService() {
        this.infracaoDAO = new InfracaoDAO();
    }
    
    /**
     * Responsável por pesquisar um veículo por sua placa
     * @param placa
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public List<Veiculo> pesquisarVeiculosPorPlaca(String placa) throws SQLException, ClassNotFoundException {
                
        try (Connection conn = Conexao.conectar()) {
            return infracaoDAO.pesquisarVeiculosPorPlaca(conn, placa);
        } 
    }
    
    /**
     * Responsável por criar uma infração no banco de dados
     * @param infracao
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public boolean criarInfracao(Infracao infracao) throws SQLException, ClassNotFoundException {
        
        try (Connection conn = Conexao.conectar()) {
            return infracaoDAO.criarInfracao(conn, infracao);
        } 
    }
}

