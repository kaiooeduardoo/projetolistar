<%-- 
    Document   : criarusuario
    Created on : 08/11/2023, 22:30:29
    Author     : Kaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulário de Usuário</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .gradient-custom {
                /* Exemplo de gradiente: de azul para verde */
                background: linear-gradient(to right, #56CCF2, #2F80ED);
                padding: 20px;
                border-radius: 8px;
                color: white;
            }
            .form-control {
                border-radius: 0.25rem;
            }
            .form-group label {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
    <div class="container mt-5">
        <div class="row justify-content-center">                
            <div class="col-md-6">
                <div class="gradient-custom">
                    <form action="CriarUsuarioController" method="post">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h2>Criar Usuário</h2>
                            <a href="listarUsuarios" class="btn btn-danger">VOLTAR</a>
                        </div>
                        <div class="form-group">
                            <label for="nome">Nome:</label>
                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o nome" required>
                        </div>
                        <div class="form-group">
                            <label for="idade">Idade:</label>
                            <input type="number" class="form-control" id="idade" name="idade" placeholder="Digite a idade" required>
                        </div>
                        <div class="form-group">
                            <label for="nome">Telefone:</label>
                            <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Digite o número do telefone" required>
                        </div>
                        <div class="form-group">
                            <label for="nome">E-mail:</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Digite o seu melhor e-mail" required>
                        </div>
                        <button type="submit" class="btn btn-light btn-block">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
