<%-- 
    Document   : index
    Created on : 27/10/2023, 22:06:52
    Author     : Kaio
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>P�gina inicial</title>
        <!-- Inclus�o do CSS do Bootstrap -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
        <div class="container mt-4">
            <h1>P�gina inicial</h1>
            <!--C�digo respons�vel por exibir mensagem de sucesso de cria��o de usu�rio - IN�CIO -->
            <c:if test="${not empty sessionScope.successMessage}">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                ${sessionScope.successMessage}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <script>
                $(".alert").alert();
            </script>
                <% session.removeAttribute("successMessage"); %> 
            </c:if>
        <!--C�digo respons�vel por exibir mensagem de sucesso de cria��o de usu�rio - FIM -->
            <a href="listarInfracoes" class="btn btn-danger">Listar Infra��es</a><br><br>
            <a href="listarUsuarios" class="btn btn-primary">Listar Usu�rios</a><br><br>
            <a href="ListUserDataTables" class="btn btn-primary">Listar Usu�rios DATATABLES</a><br>
            <a href="PaginaDoisController" class="btn btn-secondary mt-2">P�gina dois</a>
            <a href="CriarVeiculoController" class="btn btn-secondary mt-2">Criar ve�culo</a>
            <a href="ListarVeiculos" class="btn btn-secondary mt-2">Listar ve�culo</a>
            <a href="PaginaErro" class="btn btn-secondary mt-2">P�gina ERRO</a>            
        </div>
    </body>
</html>
