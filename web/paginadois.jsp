<%-- 
    Document   : paginadois
    Created on : 27/10/2023, 22:15:45
    Author     : Kaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Página dois</title>
        <!-- Inclusão do CSS do Bootstrap -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            html {
                background-color: blueviolet;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="container mt-4">
            <h1>Bem vindo a segunda página!</h1>
            <a href="PaginaUmController" class="btn btn-primary">Voltar</a>
        </div>
    </body>
</html>
