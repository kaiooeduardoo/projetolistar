<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Editar Usuário</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .gradient-custom {
            /* Mudando a paleta de cores para um gradiente de roxo para rosa */
            background: linear-gradient(to right, #D66D75, #E29587);
            padding: 20px;
            border-radius: 8px;
            color: white;
        }
        .form-control {
            border-radius: 0.25rem;
        }
        .form-group label {
            font-weight: bold;
        }
        .btn-custom {
            background-color: white;
            color: #D66D75;
        }
    </style>
</head>
<body>
<div class="container mt-5">
    <div class="row justify-content-center">                
        <div class="col-md-6">
            <div class="gradient-custom">
                <form action="EditarUsuarioController" method="post">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h2>Editar Usuário</h2>
                        <a href="listarUsuarios" class="btn btn-danger">VOLTAR</a>
                    </div>
                    <input type="hidden" name="id" value="${usuario.id}"/>
                    <div class="form-group">
                        <label for="nome">Nome:</label>
                        <input type="text" class="form-control" id="nome" name="nome" value="${usuario.nome}" required>
                    </div>
                    <div class="form-group">
                        <label for="idade">Idade:</label>
                        <input type="number" class="form-control" id="idade" name="idade" value="${usuario.idade}" required>
                    </div>
                    <!--<input type="hidden" name="idContato" value="${contato.idContato}"/>-->
                    <div class="form-group">
                        <label for="telefone">Telefone:</label>
                        <input type="text" class="form-control" id="telefone" name="telefone" value="${not empty contato.telefone ? contato.telefone : ''}" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" value="${not empty contato.email ? contato.email : ''}" required>
                    </div>
                    <button type="submit" class="btn btn-custom btn-block">Atualizar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
