<%-- 
    Document   : CriarInfracao
    Created on : 30/03/2024, 15:48:41
    Author     : Kaio
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Criar Infra��o</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .gradient-custom {
            /* Exemplo de gradiente: de azul para verde */
            background: linear-gradient(to right, #56CCF2, #2F80ED);
            padding: 20px;
            border-radius: 8px;
            color: white;
        }
        .form-control {
            border-radius: 0.25rem;
        }
        .form-group label {
            font-weight: bold;
        }
        h2 {
            text-align: center;
        }
    </style>
</head>
<body>

<h2>Criar Nova Infra��o</h2>
    <div class="container mt-5">
        <div class="row justify-content-center">                
            <div class="col-md-6">
                <input type="text" id="placaInput" placeholder="Digite a placa">
                <button id="pesquisarPlaca" class="btn btn-primary">Pesquisar Placa</button> <br>
                <div id="resultadoPesquisa"></div>
            </div>
        </div>
    </div>    

    <div class="container mt-5">
        <div class="row justify-content-center">                
            <div class="col-md-6">
                <div class="gradient-custom">                    
                    <form id="formInfracao" action="${pageContext.request.contextPath}/infracao" method="post">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h2>Cadastrar Infra��o</h2>
                            <a href="listarInfracoes" class="btn btn-danger">VOLTAR</a>
                        </div>
                        <!-- Informa��es do ve�culo selecionado - IN�CIO -->
                        <div id="veiculoInfo"></div>
                        <!-- Informa��es do ve�culo selecionado - FIM -->
                        <!-- Esse campo ser� preenchido automaticamente quando um ve�culo for selecionado -->
                        <input type="hidden" id="veiculoId" name="veiculoId">
                        <input type="hidden" id="placa" name="placa">
                        <div class="form-group">
                        Usu�rio:
                        <select name="usuarioId">
                            <c:forEach items="${usuarios}" var="usuario">
                                <option value="${usuario.id}">${usuario.nome}</option>
                            </c:forEach>
                        </select>
                        </div>
                        <div class="form-group">    
                        Observa��es:
                        <textarea name="obs"></textarea>
                        </div>
                        <input class="btn btn-light btn-block" type="submit" value="Registrar Infra��o">
                    </form>
                </div>
            </div>
        </div>   
    </div>                        

<script>
document.getElementById('pesquisarPlaca').addEventListener('click', function() {
    var placa = document.getElementById('placaInput').value.trim();
    if (!placa) {
        alert("Por favor, digite uma placa para pesquisar.");
        return; // Interrompe a execu��o da fun��o
    }
    fetch('${pageContext.request.contextPath}/pesquisarVeiculo?placa=' + placa)
        .then(function(response) {
            return response.json();
        })
        .then(function(veiculos) {
            var resultadoHTML = '<h3>Selecione um Ve�culo</h3>';
            veiculos.forEach(function(veiculo) {
                resultadoHTML += '<div onclick="selecionarVeiculo(' + veiculo.id_veiculo_locado + ', \'' + veiculo.placa + '\', \'' + veiculo.modelo + '\')" style="cursor: pointer; padding: 10px; border: 1px solid #ccc; margin-top: 5px;">' + veiculo.placa + ' - ' + veiculo.modelo + '</div>';
            });
            document.getElementById('resultadoPesquisa').innerHTML = resultadoHTML;
        });
});

function selecionarVeiculo(idVeiculo, placa, modelo) {
    document.getElementById('veiculoId').value = idVeiculo;
    document.getElementById('placa').value = placa;
    document.getElementById('veiculoInfo').innerHTML = 'Ve�culo Selecionado: <strong>Placa:</strong> ' + placa + ', <strong>Modelo:</strong> ' + modelo;
    // Limpa os resultados da pesquisa ap�s a sele��o
    document.getElementById('resultadoPesquisa').innerHTML = '';
}

//Valida��o para submeter o formul�rio apenas se tiver escolhido um ve�culo
document.getElementById('formInfracao').addEventListener('submit', function(event) {
    var veiculoId = document.getElementById('veiculoId').value;
    if (!veiculoId) {
        alert("Por favor, selecione um ve�culo antes de submeter o formul�rio.");
        event.preventDefault(); // Impede a submiss�o do formul�rio
    }
});

</script>

</body>
</html>


