<%-- 
    Document   : erro
    Created on : 12/11/2023, 18:26:28
    Author     : Kaio
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Page Error</title>
    </head>
    <body>
        <h1>Erro!</h1>
        <p>Falha ao tentar realizar opera��o!</p>
            <!--C�digo respons�vel por exibir mensagem de sucesso de cria��o de usu�rio - IN�CIO -->
            <c:if test="${not empty sessionScope.errorMessage}">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                ${sessionScope.errorMessage}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <script>
                $(".alert").alert();
            </script>
                <% session.removeAttribute("errorMessage"); %> 
            </c:if>
        <!--C�digo respons�vel por exibir mensagem de sucesso de cria��o de usu�rio - FIM -->
    </body>
</html>
