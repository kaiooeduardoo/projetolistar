<%-- 
    Document   : lista
    Created on : 27/10/2023, 22:09:22
    Author     : Kaio
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Lista de Ve�culos</title>

    <!-- Inclus�o dos icones do Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" rel="stylesheet">    
    
    <!-- Inclus�o do CSS DataTables -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css">
    
        <!-- Inclus�o dos Scripts DATATABLES -->
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>

</head>
<body>
    <div class="container mt-4">
        <a href="PaginaUmController" class="btn btn-primary">VOLTAR</a>
        <a href="CriarVeiculoController" class="btn btn-success">CRIAR VE�CULO</a>        
    </div>
    <div class="container mt-4">
        <!--<h2>Lista de ve�culos: <a href="ImprimirUsuario" title='Imprimir Usu�rios'><i class="btn btn-danger fa-solid fa-print"></i></a></h2>-->
        
        <!--C�digo respons�vel por exibir mensagem de sucesso de cria��o de ve�culo - IN�CIO -->
        <c:if test="${not empty sessionScope.successMessage}">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            ${sessionScope.successMessage}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <script>
            $(".alert").alert();
        </script>
            <% session.removeAttribute("successMessage"); %> 
        </c:if>
        <!--C�digo respons�vel por exibir mensagem de sucesso de cria��o de usu�rio - FIM -->
        
        <table class="table" id="tabela-veiculos">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Placa</th>
                    <th>Modelo</th>
                    <th>A��es</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="veiculo" items="${veiculos}">
                    <tr>
                        <td>${veiculo.id_veiculo_locado}</td>
                        <td>${veiculo.placa}</td>
                        <td>${veiculo.modelo}</td>
                        <td>
                            <a href="EditarUsuarioController?id=${veiculo.id_veiculo_locado}" class="btn btn-info"><i class="fas fa-edit"></i> Editar</a>
                        </td>
                    </tr>
                </c:forEach> 
            </tbody>
        </table>       
    </div>
    
    
    


    
    <script>
      /*  $(document).ready(function() {
            $('#tabela-usuarios').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "/ListUserDataTables",
                    "type": "GET"
                },
                "columns": [
                    { "data": 0 }, // Assumindo que 0 � o �ndice para 'nome'
                    { "data": 1 }  // Assumindo que 1 � o �ndice para 'idade'
                    // Adicione mais colunas conforme necess�rio
                ]
            });
        });*/
    new DataTable('tabela-veiculos', {
        displayStart: 20
    });
    </script>



    
</body>
</html>
