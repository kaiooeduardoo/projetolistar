<%-- 
    Document   : CriarVeiculo
    Created on : 30/03/2024, 13:09:46
    Author     : Kaio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulário de Veículo</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .gradient-custom {
                /* Exemplo de gradiente: de azul para verde */
                background: linear-gradient(to right, #56CCF2, #2F80ED);
                padding: 20px;
                border-radius: 8px;
                color: white;
            }
            .form-control {
                border-radius: 0.25rem;
            }
            .form-group label {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
    <div class="container mt-5">
        <div class="row justify-content-center">                
            <div class="col-md-6">
                <div class="gradient-custom">
                    <form action="CriarVeiculoController" method="post">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h2>Criar Veículo</h2>
                            <a href="PaginaUmController" class="btn btn-danger">VOLTAR</a>
                        </div>
                        <div class="form-group">
                            <label for="nome">Placa:</label>
                            <input type="text" class="form-control" id="placa" name="placa" placeholder="Digite a placa" required>
                        </div>                        
                        <div class="form-group">
                            <label for="nome">Modelo:</label>
                            <input type="text" class="form-control" id="modelo" name="modelo" placeholder="Digite o modelo do veículo" required>
                        </div>
                        
                        <button type="submit" class="btn btn-light btn-block">Salvar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
